import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private http: HttpClient) {}

  contactForm(contactInfo: any): Observable<any> {
    return this.http.post<Object>('url', contactInfo, this.httpOptions)
    .pipe(
      map(res => res),
      catchError(err => err)
    );

  }
}
