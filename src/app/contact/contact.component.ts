import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ContactService } from './contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  contactForm = this.fb.group({
    name: ['', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(15)
    ]],
    surname: ['', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(15)
    ]],
    email: ['', [
      Validators.required,
      Validators.email,
    ]],
    phone: ['', [
      Validators.required,
      Validators.pattern(/[6-7]{1}[0-9]{8}/)
    ]],
    reason:['', [
      Validators.required,
      Validators.minLength(15),
      Validators.maxLength(100)
    ]]
  });

  constructor(private fb: FormBuilder, private contactService: ContactService) {}

  ngOnInit(): void {
  }

  onSubmit = () => {
    if (this.contactForm.valid) {
      this.contactService.contactForm(this.contactForm.value).subscribe(
        data => console.log(data),
        err => console.log(err)
      );
    }
  }

}
