import { Component, OnInit } from '@angular/core';
import { Post } from '../post';
import { HomeService } from './home.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  posts: Post[];
  selectedPost;

  constructor(private homeService: HomeService) {

  }

  ngOnInit(): void {
    this.getPost();
  }

  selectPost = (post: Post) => this.selectedPost = post;

  getPost(): void {
    this.homeService.getPosts().subscribe(
      data => this.posts = data,
      err => console.log(err)
    );
  }

}
