import { Injectable } from '@angular/core';
import { Post } from '../post';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class HomeService {

  private postsUrl = 'https://jsonplaceholder.typicode.com/posts';

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  constructor(
    private http: HttpClient
  ) { }

  getPosts = (): Observable<Post[]> => {
    return this.http.get<Post[]>(this.postsUrl)
    .pipe(
      map(res => res),
      catchError(this.handleError<Post[]>('getHeroes', []))
    );
  }
}
