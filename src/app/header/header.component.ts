import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  name = "Posts App";

  links = [
    {
      id: 1,
      name: 'Inicio',
      link: '/'
    },
    {
      id: 1,
      name: 'Posts',
      link: '/posts'
    },
    {
      id: 2,
      name: 'Contacto',
      link: '/contacto'
    }
  ];
  
  constructor() { }

  ngOnInit(): void {
  }

}
