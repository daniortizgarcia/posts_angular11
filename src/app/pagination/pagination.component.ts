import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input() paginationNumber: number;
  @Input() actualPage: number;

  @Output() changePage: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  counter = (i: number) => {
    return new Array(i);
  }

  selectPage = (page: number) => {
    this.changePage.emit(page);
  }

}
