import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Post } from '../post';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  @Input() posts: Post[];
  @Input() paginationNumbersShow: number;

  @Output() onSelectPost: EventEmitter<Post> = new EventEmitter();

  postsBackup: Post[];
  paginationNumber: number;
  paginationNumberSelected: number;
  numberOfPagition: number;

  constructor() { }

  ngOnInit(): void {
    this.postsBackup = this.posts;
    this.paginationNumberSelected = 0;
    this.pagination();
  }

  public selectPost = (post) => this.onSelectPost.emit(post);

  pagination = () => {
    const firstPost = this.paginationNumberSelected * this.paginationNumbersShow;
    this.posts = this.postsBackup.slice(firstPost, firstPost + this.paginationNumbersShow);
    if (!this.paginationNumber) {
      this.paginationNumber =  Math.ceil(this.postsBackup.length / this.posts.length); 
    }
  }

  changePagination = (page: number) => {
    this.paginationNumberSelected = page;
    this.pagination();
  }

}
